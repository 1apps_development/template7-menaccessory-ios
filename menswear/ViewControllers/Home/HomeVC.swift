import UIKit
import SwiftyJSON

class HomeTableviewCell: UITableViewCell {
  @IBOutlet weak var img_item: UIImageView!
  @IBOutlet weak var lbl_itemname: UILabel!
  @IBOutlet weak var lbl_price: UILabel!
  @IBOutlet weak var btn_cart: UIButton!
  @IBOutlet weak var lbl_curruncy : UILabel!
  @IBOutlet weak var lbl_tag : UILabel!
  @IBOutlet weak var btn_favrites : UIButton!
  @IBOutlet weak var lbl_variant: UILabel!
}

class HomeCollectionviewCell: UICollectionViewCell {
  @IBOutlet weak var img_item: UIImageView!
  @IBOutlet weak var lbl_itemname: UILabel!
  @IBOutlet weak var lbl_price: UILabel!
  @IBOutlet weak var btn_cart: UIButton!
  @IBOutlet weak var lbl_curruncy : UILabel!
  @IBOutlet weak var lbl_tag : UILabel!
  @IBOutlet weak var btn_favrites : UIButton!
  @IBOutlet weak var lbl_variant: UILabel!
}

class HomeCategoryCollectionviewCell: UICollectionViewCell {
  @IBOutlet weak var lbl_title: UILabel!
  @IBOutlet weak var view_bg: UIView!
}

class HomeVC: UIViewController, UIScrollViewDelegate {
  
  // MARK: - IBOutlets
  @IBOutlet weak var collectionview_category: UICollectionView!
  @IBOutlet weak var collectionview_category_height : NSLayoutConstraint!
  @IBOutlet weak var collectionviewProducts_height: NSLayoutConstraint!
  @IBOutlet weak var collectionview_products: UICollectionView!
  @IBOutlet weak var tableviewProducts_height: NSLayoutConstraint!
  @IBOutlet weak var tableview_products: UITableView!
  @IBOutlet weak var tableviewProducts_height2: NSLayoutConstraint!
  @IBOutlet weak var tableview_products2: UITableView!
  @IBOutlet weak var tableviewProducts_height3: NSLayoutConstraint!
  @IBOutlet weak var tableview_products3: UITableView!
  @IBOutlet weak var ScrollView: UIScrollView!
  @IBOutlet weak var lbl_count: UILabel!
  @IBOutlet weak var homepage_header_title_text1: UILabel!
  @IBOutlet weak var homepage_header_sub_text1: UILabel!
  @IBOutlet weak var homepage_header_btn1: UIButton!
  @IBOutlet weak var homepage_feature_products_heading: UILabel!
  @IBOutlet weak var homepage_feature_products_title_text: UILabel!
  @IBOutlet weak var homepage_products_heading: UILabel!
  @IBOutlet weak var homepage_products_title_text: UILabel!
  @IBOutlet weak var homepage_trending_products_heading: UILabel!
  @IBOutlet weak var homepage_trending_products_title_text: UILabel!
  @IBOutlet weak var homepage_newsletter_title_text: UILabel!
  @IBOutlet weak var homepage_newsletter_sub_text: UILabel!
  @IBOutlet weak var view_Empty : UIView!
  @IBOutlet weak var img_trending: UIImageView!
  @IBOutlet weak var lbl_trending: UILabel!
  @IBOutlet weak var btn_checkMark: UIButton!

  // MARK: - variables
  var Categories_Array = [JSON]()
  var Trending_Categories_Array = [JSON]()
  var main_category_id_trending = String()
  var Home_Categories_Array = [[String:String]]()
  var pageIndex = 1
  var lastIndex = 0
  var pageIndex_best = 1
  var lastIndex_best = 0
  var pageIndex_trending = 1
  var lastIndex_trending = 0
  var pageIndex_maincategory = 1
  var lastIndex_maincategory = 0
  var SelectedCategoryid = String()
  var SelectedSubCategoryid = String()
  var Featured_Products_Array = [[String:String]]()
  var Bestseller_Products_Array = [[String:String]]()
  var Trending_Products_Array = [[String:String]]()
  var Trending_Products_Categoires_Array = [[String:String]]()
  var selectedindex = 0
  var selectedindex_Trending = 0
  var product_id = String()
  var Selected_Variant_id = String()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.ScrollView.delegate = self
  }
  
  // MARK: - viewwillAppear
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
      let Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
      UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
      cornerRadius(viewName: self.lbl_count, radius: self.lbl_count.frame.height / 2)
    }
    let urlString = BASE_URL
    let headers:NSDictionary = ["Content-type": "application/json"]
    let params: NSDictionary = ["theme_id":APP_THEME]
    self.Webservice_baseURL(url: urlString, params: params, header: headers)
  }
  @IBAction func btnTap_checkMark(_ sender: UIButton) {
    if self.btn_checkMark.imageView?.image == UIImage(named: "ic_chk") {
      self.btn_checkMark.setImage(UIImage(named: "ic_squarefill"), for: .normal)
    }
    else {
      self.btn_checkMark.setImage(UIImage(named: "ic_chk"), for: .normal)
    }
  }



  @IBAction func btnTap_Menu_Clicked(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }
  
  @IBAction func btnTap_Cart(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }
  @IBAction func btnTap_Search(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

extension HomeVC: UITableViewDelegate,UITableViewDataSource {
  
  // MARK: - numberOfRowsInSection
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableview_products {
      return self.Featured_Products_Array.count
    }
    else if tableView == self.tableview_products2 {
      return self.Bestseller_Products_Array.count
    }
    else if tableView == self.tableview_products3 {
      return self.Trending_Products_Categoires_Array.count
    }
    return 0
  }
  
  // MARK: - heightForRowAt
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableView == self.tableview_products {
      return 180
    }
    if tableView == self.tableview_products2 {
      return 180
    }
    else if tableView == self.tableview_products3 {
      return 180
    }
    return 0
  }
  
  // MARK: - cellForRowAt
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == self.tableview_products {
      let cell = self.tableview_products.dequeueReusableCell(withIdentifier: "HomeTableviewCell") as! HomeTableviewCell
      let data = Featured_Products_Array[indexPath.item]
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_price.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_cart), for: .touchUpInside)
      return cell
    }
    else if tableView == self.tableview_products2 {
      let cell = self.tableview_products2.dequeueReusableCell(withIdentifier: "HomeTableviewCell") as! HomeTableviewCell
      let data = self.Bestseller_Products_Array[indexPath.item]
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_price.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_best), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_best), for: .touchUpInside)
      return cell
    }
    else {
      let cell = self.tableview_products3.dequeueReusableCell(withIdentifier: "HomeTableviewCell") as! HomeTableviewCell
      let data = Trending_Products_Categoires_Array[indexPath.item]
      print(data.count)
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_price.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_Trending_Product), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_cart_Trending_Product), for: .touchUpInside)
      return cell
    }
  }

  // MARK: - didSelectRowAt
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if tableView == self.tableview_products {
      let data = self.Featured_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
    else if tableView == self.tableview_products2 {
      let data = self.Bestseller_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
    else if tableView == self.tableview_products3 {
      let data = self.Trending_Products_Categoires_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }

  // MARK: - like button action
  @objc func btnTap_Like_Trending_Product(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = Trending_Products_Categoires_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "TrendingProduct")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "TrendingProduct")
      }
    }
  }
  // MARK: - like button action
  @objc func btnTap_Like(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = Featured_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "Featured")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "Featured")
      }
    }
  }
  // MARK: - bestseller cart button action
  @objc func btnTap_Cart_best(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Bestseller_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)

          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }

          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Bestseller_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }
  // MARK: - bestsellers like button action
  @objc func btnTap_Like_best(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = self.Bestseller_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "best")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "best")
      }
    }
  }
  // MARK: - cart button action
  @objc func btnTap_cart(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Featured_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }
          
          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Featured_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }
  // MARK: - cart button action
  @objc func btnTap_cart_Trending_Product(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Trending_Products_Categoires_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }

          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Trending_Products_Categoires_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }
}
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
  // MARK: - numberOfItemsInSection (Collection)
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView == self.collectionview_category {
      return self.Home_Categories_Array.count
    }
    else if collectionView == self.collectionview_products {
      return self.Trending_Products_Array.count
    }
    return 0
  }
  
  // MARK: - cellForItemAt (Collection)
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == self.collectionview_category {
      let cell = self.collectionview_category.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCollectionviewCell", for: indexPath) as! HomeCategoryCollectionviewCell
      let data = self.Home_Categories_Array[indexPath.item]
      cell.lbl_title.text = data["name"]!.uppercased()
      if indexPath.row == self.selectedindex {
        cell.view_bg.backgroundColor = UIColor.init(named: "App_Color")
        cell.lbl_title.textColor = UIColor.init(named: "App_bg_Color")
      }
      else {
        cell.view_bg.backgroundColor = UIColor.init(named: "App_bg_Color")
        cell.lbl_title.textColor = UIColor.white
      }
      return cell
    }
    else {
      let cell = self.collectionview_products.dequeueReusableCell(withReuseIdentifier: "HomeCollectionviewCell", for: indexPath) as! HomeCollectionviewCell
      let data = self.Trending_Products_Array[indexPath.item]
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_price.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_Trending), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_Trending), for: .touchUpInside)
      return cell
    }
  }
  
  // MARK: - didSelectItemAt (Collection)
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView == self.collectionview_category {
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        self.pageIndex_trending = 1
        self.lastIndex_trending = 0
        let data = self.Home_Categories_Array[indexPath.item]
        let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex_trending)"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        self.main_category_id_trending = data["category_id"]!
        let params: NSDictionary = ["maincategory_id":self.main_category_id_trending,
                                    "theme_id":APP_THEME]
        self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
      }
      else {
        self.pageIndex_trending = 1
        self.lastIndex_trending = 0
        let data = self.Home_Categories_Array[indexPath.item]
        let urlString = API_URL + "categorys-product?page=\(self.pageIndex_trending)"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        self.main_category_id_trending = data["category_id"]!
        let params: NSDictionary = ["maincategory_id":self.main_category_id_trending,
                                    "theme_id":APP_THEME]
        self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
      }
      self.selectedindex = indexPath.item
      self.collectionview_category.reloadData()
    }
    else {
      let data = self.Trending_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  // MARK: - will display
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,forItemAt indexPath: IndexPath) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      if collectionView == self.collectionview_products {
        if indexPath.item == self.Trending_Products_Array.count - 1 {
          if self.pageIndex_trending != self.lastIndex_trending {
            self.pageIndex_trending = self.pageIndex_trending + 1
            if self.Trending_Products_Array.count != 0 {
              let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex_trending)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["maincategory_id":main_category_id_trending,
                                          "theme_id":APP_THEME]
              self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
            }
          }
        }
      }
    }
    else {
      if collectionView == self.collectionview_products {
        if indexPath.item == self.Trending_Products_Array.count - 1 {
          if self.pageIndex_trending != self.lastIndex_trending {
            self.pageIndex_trending = self.pageIndex_trending + 1
            if self.Trending_Products_Array.count != 0 {
              let urlString = API_URL + "categorys-product?page=\(self.pageIndex_trending)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["maincategory_id":main_category_id_trending,
                                          "theme_id":APP_THEME]
              self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
            }
          }
        }
      }
    }
  }
  // MARK: - layout (Collection)
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == self.collectionview_category {
      return CGSize(width: (UIScreen.main.bounds.width - 50) / 2, height: 50)
    }
    else {
      return CGSize(width: (UIScreen.main.bounds.width - 48.0) / 2, height: ((UIScreen.main.bounds.width - 48.0) / 2) * 1.5)
    }
  }
  // MARK: - trending Cart Button action
  @objc func btnTap_Cart_Trending(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Trending_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]!  {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          
          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }
          
          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Trending_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"variant_id":data["default_variant_id"]!,"qty":"1","product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }
  // MARK: - Trending Cart button action
  @objc func btnTap_Like_Trending(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = self.Trending_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "trending")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "trending")
      }
    }
  }
}

extension HomeVC {
  func Webservice_baseURL(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"
      {
        let jsondata = jsonResponse!["data"].dictionaryValue
        IMG_URL = jsondata["image_url"]!.stringValue
        API_URL = "\(jsondata["base_url"]!.stringValue)/"
        PAYMENT_URL = "\(jsondata["payment_url"]!.stringValue)/"
        

        let urlString = API_URL + "landingpage"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_landingpage(url: urlString, params: params, header: headers)

      }
      else if status == "9"
      {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else
      {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - landing page api calling
  func Webservice_landingpage(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"]["them_json"].dictionaryValue
        
        // home-header
        let home_header = jsondata["homepage-header"]!.dictionaryValue
        self.homepage_header_title_text1.text = home_header["homepage-header-title-text1"]!.stringValue
        self.homepage_header_sub_text1.text = home_header["homepage-header-sub-text1"]!.stringValue
        //self.homepage_header_btn1.setTitle(home_header["homepage-header-btn1"]!.stringValue, for: .normal)
        
        // homepage-feature-products
        let homepage_feature_products = jsondata["homepage-feature-products"]!.dictionaryValue
        self.homepage_feature_products_heading.text = homepage_feature_products["homepage-feature-products-heading"]!.stringValue
        self.homepage_feature_products_title_text.text = homepage_feature_products["homepage-feature-products-title-text"]!.stringValue.uppercased()
        
        // homepage-products
        let homepage_products = jsondata["homepage-products"]!.dictionaryValue
        self.homepage_products_heading.text = homepage_products["homepage-products-heading"]!.stringValue
        self.homepage_products_title_text.text = homepage_products["homepage-products-title-text"]!.stringValue.uppercased()
        
        // homepage-trending-products
        let homepage_trending_products = jsondata["homepage-trending-products"]!.dictionaryValue
        self.homepage_trending_products_heading.text = homepage_trending_products["homepage-trending-products-heading"]!.stringValue
        self.homepage_trending_products_title_text.text = homepage_trending_products["homepage-trending-products-title-text"]!.stringValue.uppercased()
        
        // homepage-newsletter
        let homepage_newsletter = jsondata["homepage-newsletter"]!.dictionaryValue
        self.homepage_newsletter_title_text.text = homepage_newsletter["homepage-newsletter-title-text"]!.stringValue.uppercased()
        self.homepage_newsletter_sub_text.text = homepage_newsletter["homepage-newsletter-sub-text"]!.stringValue
        
        // Currency
        let urlString1 = API_URL + "currency"
        let headers1:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params1: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_currency(url: urlString1, params: params1, header: headers1)
        
        let urlString5 = API_URL + "home-categoty"
        let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params5: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_category(url: urlString5, params: params5, header: headers5)

        let urlString2 = API_URL + "tranding-category"
        let headers2:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params2: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_Trendingcategory(url: urlString2, params: params2, header: headers2)
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
          self.pageIndex = 1
          self.lastIndex = 0
          let urlString = API_URL + "tranding-category-product-guest?page=\(self.pageIndex)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["maincategory_id":self.main_category_id_trending,
                                      "theme_id":APP_THEME]
          self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)

          self.pageIndex_best = 1
          self.lastIndex_best = 0
          let urlString4 = API_URL + "bestseller-guest?page=\(self.pageIndex_best)"
          let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params4: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)

          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let urlString5 = API_URL + "categorys-product-guest?page=\(self.pageIndex_trending)"
          let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params5: NSDictionary = ["maincategory_id":"",
                                       "theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts(url: urlString5, params: params5, header: headers5)
        }
        else {
          self.pageIndex = 1
          self.lastIndex = 0
          let urlString = API_URL + "tranding-category-product?page=\(self.pageIndex)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["maincategory_id":"",
                                      "theme_id":APP_THEME]
          self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)

          self.pageIndex_best = 1
          self.lastIndex_best = 0
          let urlString4 = API_URL + "bestseller?page=\(self.pageIndex_best)"
          let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params4: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)

          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let urlString5 = API_URL + "categorys-product?page=\(self.pageIndex_trending)"
          let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params5: NSDictionary = ["maincategory_id":"",
                                       "theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts(url: urlString5, params: params5, header: headers5)
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - currency api calling
  func Webservice_currency(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        UserDefaultManager.setStringToUserDefaults(value: jsondata["currency"]!.stringValue, key: UD_currency)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["currency_name"]!.stringValue, key: UD_currency_Name)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Categorysproduct api calling
  func Webservice_Categorysproduct(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex == 1 {
          self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
          self.Featured_Products_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "tag_api":data["tag_api"].stringValue,
                            "cover_image_path":data["cover_image_path"].stringValue,
                            "final_price":data["final_price"].stringValue,
                            "in_whishlist":data["in_whishlist"].stringValue,
                            "default_variant_id":data["default_variant_id"].stringValue,
                            "orignal_price":data["original_price"].stringValue,
                            "discount_price":data["discount_price"].stringValue,
                            "variant_name":data["default_variant_name"].stringValue]
          self.Featured_Products_Array.append(productObj)
        }
        self.view_Empty.isHidden = true
        self.tableview_products.reloadData()
        self.tableview_products.dataSource = self
        self.tableview_products.delegate = self
        self.tableviewProducts_height.constant = CGFloat(self.Featured_Products_Array.count*180)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Trendingprodcuts api calling
  func Webservice_Trendingprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex_trending == 1 {
          self.lastIndex_trending = Int(jsondata["last_page"]!.stringValue)!
          self.Trending_Products_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "tag_api":data["tag_api"].stringValue,
                            "cover_image_path":data["cover_image_path"].stringValue,
                            "final_price":data["final_price"].stringValue,
                            "in_whishlist":data["in_whishlist"].stringValue,
                            "default_variant_id":data["default_variant_id"].stringValue,
                            "orignal_price":data["original_price"].stringValue,
                            "discount_price":data["discount_price"].stringValue,
                            "variant_name":data["default_variant_name"].stringValue]
          self.Trending_Products_Array.append(productObj)
        }
        self.collectionview_products.reloadData()
        self.collectionview_products.delegate = self
        self.collectionview_products.dataSource = self
        self.collectionviewProducts_height.constant = ((UIScreen.main.bounds.width - 48.0) / 2) * 1.5
      }
      else if status == "9"  {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }

  // MARK: - Trendingprodcuts api calling
  func Webservice_Trendingprodcuts_Categoires(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex_trending == 1 {
          self.lastIndex_trending = Int(jsondata["last_page"]!.stringValue)!
          self.Trending_Products_Categoires_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "tag_api":data["tag_api"].stringValue,
                            "cover_image_path":data["cover_image_path"].stringValue,
                            "final_price":data["final_price"].stringValue,
                            "in_whishlist":data["in_whishlist"].stringValue,
                            "default_variant_id":data["default_variant_id"].stringValue,
                            "orignal_price":data["original_price"].stringValue,
                            "discount_price":data["discount_price"].stringValue,
                            "variant_name":data["default_variant_name"].stringValue]
          self.Trending_Products_Categoires_Array.append(productObj)
        }
        self.tableview_products3.reloadData()
        self.tableview_products3.delegate = self
        self.tableview_products3.dataSource = self
        self.tableviewProducts_height3.constant = CGFloat(self.Trending_Products_Categoires_Array.count * 180)
      }
      else if status == "9"  {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Cart api calling
  func Webservice_Cart(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["data"]["count"].stringValue, key: UD_CartCount)
        self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        let alert = UIAlertController(title: nil, message: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"), preferredStyle: .alert)
        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
          self.dismiss(animated: true)
        }
        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
          let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
          self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(photoLibraryAction)
        alert.addAction(cameraAction)
        self.present(alert, animated: true, completion: nil)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else if status == "0" {
        let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
          let urlString = API_URL + "cart-qty"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                      "product_id":self.product_id,
                                      "variant_id":self.Selected_Variant_id,
                                      "quantity_type":"increase",
                                      "theme_id":APP_THEME]
          self.Webservice_CartQty(url: urlString, params: params, header: headers)
        }
        let noAction = UIAlertAction(title: "No", style: .destructive)
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        self.present(alertVC,animated: true,completion: nil)
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - CartQty
  func Webservice_CartQty(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  //MARK: - wishlist api calling
  func Webservice_wishlist(url:String, params:NSDictionary,header:NSDictionary,wishlisttype:String,sender:Int,isselect:String) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        if isselect == "best" {
          if wishlisttype == "add" {
            var data = self.Bestseller_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Bestseller_Products_Array.remove(at: sender)
            self.Bestseller_Products_Array.insert(data, at: sender)
            self.tableview_products2.reloadData()
          }
          else {
            var data = self.Bestseller_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Bestseller_Products_Array.remove(at: sender)
            self.Bestseller_Products_Array.insert(data, at: sender)
            self.tableview_products2.reloadData()
          }
        }
        if isselect == "TrendingProduct" {
          if wishlisttype == "add" {
            var data = self.Trending_Products_Categoires_Array[sender]
            data["in_whishlist"]! = "true"
            self.Trending_Products_Categoires_Array.remove(at: sender)
            self.Trending_Products_Categoires_Array.insert(data, at: sender)
            self.tableview_products3.reloadData()
          }
          else {
            var data = self.Trending_Products_Categoires_Array[sender]
            data["in_whishlist"]! = "false"
            self.Trending_Products_Categoires_Array.remove(at: sender)
            self.Trending_Products_Categoires_Array.insert(data, at: sender)
            self.tableview_products3.reloadData()
          }
        }
        if isselect == "trending" {
          if wishlisttype == "add" {
            var data = self.Trending_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Trending_Products_Array.remove(at: sender)
            self.Trending_Products_Array.insert(data, at: sender)
            self.collectionview_products.reloadData()
          }
          else {
            var data = self.Trending_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Trending_Products_Array.remove(at: sender)
            self.Trending_Products_Array.insert(data, at: sender)
            self.collectionview_products.reloadData()
          }
        }
        else {
          if wishlisttype == "add" {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.tableview_products.reloadData()
          }
          else {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.tableview_products.reloadData()
          }
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - category api calling
  func Webservice_category(url:String, params:NSDictionary, header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex == 1 {
          self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
          self.Home_Categories_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "image_path":data["image_path"].stringValue,
                            "status":data["status"].stringValue,
                            "category_id":data["category_id"].stringValue,
                            "category_item":data["category_item"].stringValue,
                            "icon_path":data["icon_path"].stringValue]
          self.Home_Categories_Array.append(productObj)
        }
        self.collectionview_category.delegate = self
        self.collectionview_category.dataSource = self
        self.collectionview_category.reloadData()
        if self.Home_Categories_Array.count % 2 == 0  {
          self.collectionview_category_height.constant = CGFloat((self.Home_Categories_Array.count / 2)) * 70
        }
        else {
          self.collectionview_category_height.constant = CGFloat((((self.Home_Categories_Array.count - 1) / 2) + 1)) * 90
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Bestsellerprodcuts api calling
  func Webservice_Bestsellerprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
          self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        }
        else {
          UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
          self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        }
        if self.pageIndex_best == 1 {
          self.lastIndex_best = Int(jsondata["last_page"]!.stringValue)!
          self.Bestseller_Products_Array.removeAll()
        }
        if self.pageIndex_best == self.lastIndex_best {
        }
        else {
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "tag_api":data["tag_api"].stringValue,
                            "cover_image_path":data["cover_image_path"].stringValue,
                            "final_price":data["final_price"].stringValue,
                            "in_whishlist":data["in_whishlist"].stringValue,
                            "default_variant_id":data["default_variant_id"].stringValue,
                            "orignal_price":data["original_price"].stringValue,
                            "discount_price":data["discount_price"].stringValue,
                            "variant_name":data["default_variant_name"].stringValue]
          self.Bestseller_Products_Array.append(productObj)
        }
        self.view_Empty.isHidden = true
        self.tableview_products2.delegate = self
        self.tableview_products2.dataSource = self
        self.tableview_products2.reloadData()
        self.tableviewProducts_height2.constant = CGFloat(self.Bestseller_Products_Array.count*180)
        if self.Bestseller_Products_Array.count % 2 == 0 {
        }
        else {
        }
        let urlString = API_URL + "extra-url"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_Extraurl(url: urlString, params: params, header: headers)
      }
      else if status == "9"  {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - extra url api calling
  func Webservice_Extraurl(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        print(jsondata)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["contact_us"]!.stringValue, key: UD_ContactusURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["terms"]!.stringValue, key: UD_TermsURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["youtube"]!.stringValue, key: UD_YoutubeURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["messanger"]!.stringValue, key: UD_MessageURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["insta"]!.stringValue, key: UD_InstaURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["twitter"]!.stringValue, key: UD_TwitterURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["return_policy"]!.stringValue, key: UD_ReturnPolicyURL)
        //self.view_Empty.isHidden = false
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Trendingcategory api calling
  func Webservice_Trendingcategory(url:String, params:NSDictionary, header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].arrayValue
        self.Trending_Categories_Array = jsondata
        self.img_trending.sd_setImage(with: URL(string: IMG_URL + self.Trending_Categories_Array[0]["image_path"].stringValue), placeholderImage: UIImage(named: ""))
        self.lbl_trending.text = self.Trending_Categories_Array[0]["name"].stringValue

        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""  {
          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let urlString = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          self.main_category_id_trending = self.Trending_Categories_Array[0]["id"].stringValue
          let params: NSDictionary = ["main_category_id":self.main_category_id_trending,
                                      "theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts_Categoires(url: urlString, params: params, header: headers)
        }
        else {
          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let urlString = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["main_category_id":self.Trending_Categories_Array[0]["id"].stringValue,
                                      "theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts_Categoires(url: urlString, params: params, header: headers)
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}
